---
title: Binding Pocket Comparison
author: gbret
date: 2024-06-20
keywords:
---

# Binding Pocket Comparison


## Install
### Data
```sh
cd ~/hackathon/data
wget 
```


### openeye
* Already installed in "/usr/local/chm/openeye"


### Shaper
```sh
cd ~/hackathon/opt/
wget https://gitlab.com/CrtomirP/hackaton_cheminfo_2024/-/raw/main/3.%20MATERIAL/2.%20TUTORIALS/3.2%20BINDING%20POCKET%20COMPARISON/Shaper.tar.gz
tar -xzf Shaper.tar.gz
```

### siteAlign
```sh
mkdir ~/.siteAlign
cp license.txt ~/.siteAlign/licence.txt

cd ~/hackathon/opt/
wget https://gitlab.com/CrtomirP/hackaton_cheminfo_2024/-/raw/main/3.%20MATERIAL/2.%20TUTORIALS/3.2%20BINDING%20POCKET%20COMPARISON/SiteAlign.tar.gz
tar -xzf SiteAlign.tar.gz
```

### Procare
```
https://github.com/kimeguida/ProCare
```

## Configure
### Openeye
```sh
export OE_DIR=/usr/local/chm/openeye
export OE_LICENSE=${OE_DIR}/oe_license.txt
export PATH="${PATH:+${PATH}:}${OE_DIR}/bin"
```

### Procare



## Tutorial
```sh
cd ~/hackathon/data/
wget https://gitlab.com/CrtomirP/hackaton_cheminfo_2024/-/raw/main/3.%20MATERIAL/2.%20TUTORIALS/3.2%20BINDING%20POCKET%20COMPARISON/Binding_Pocket_Comparison.tar.gz
tar -xzf Binding_Pocket_Comparison.tar.gz
```

