# Task 1: Explore CMDOCK

## Group 1

- Poulet
- Ihechu
- Krokhina
- Raffaud

Main coordinator: Marko

## Overview

This section of the repository is dedicated to the CMDock task, which involves the preparation of a compilation platform, proposing libraries for Interaction Fingerprints, and working with known targets and ligands.

## Compilation Platform

- **Objective**: Prepare a robust compilation platform for CMDock.
- **Language**: C++
- **IDE**: Windows - Visual Studio

## Interaction Fingerprints

- **Objective**: Propose libraries suitable for Interaction Fingerprints that align with the primitives of the CMDock project.
- **Deployment**: As a Command Line Interface (CLI).
- **Output**: Plain text file format, supporting JSON/XML for interoperability.

## Docking Data

- **Access**: Sample docking data will be provided, along with access to databases such as PDB, DrugBank, and BindingDB.
- **Debugging**: Participants are expected to debug and ensure the functionality of CMDock.
- **Analysis**: Link the docking results to docking pause analysis (minimum of 3 persons required).

## Testing

- **Unit Test**: Implement "unit" tests to validate individual components.
- **Convergence Test**: Prepare targets with known output and reference results for comparison on a local install (minimum of 1 person required).

## Documentation

- **CLI Documentation**: Document the CLI usage, including pre-requisites (memory, CPU, GPU, System version, libraries), installation process, running the software, and interpreting the results (minimum of 1 person required).

## Tasks

1. **Prepare a Compilation Platform**: Set up the development environment and ensure all necessary tools and libraries are installed.
2. **Propose Libraries for Interaction Fingerprints**: Identify and integrate libraries that can generate Interaction Fingerprints from CMDock's output.
3. **Sample Docking Data**: Utilize provided sample data to test CMDock's performance and accuracy.
4. **Link to Docking Pause Analysis**: Develop a method to analyze docking pauses and improve CMDock's efficiency.
5. **Deploy as CLI**: Ensure CMDock can be operated via CLI and outputs are generated in the desired format.
6. **Testing**: Conduct thorough testing to validate the functionality and accuracy of CMDock.
7. **Documentation**: Create comprehensive documentation for users to understand how to install, run, and use CMDock effectively.

## Contribution

Your contributions are valuable to us. Please submit pull requests with your proposed changes or additions to the CMDock task.

## Support

For any queries or support related to the CMDock task, please reach out to the organizing committee.
