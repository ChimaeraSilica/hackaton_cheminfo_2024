# Task 4: Chemical Hazard Assessment (CHA)

## GROUP 4: CHA

- Godec
- Lee
- Queiroz Santiago
- Iwan
- **Main coordinator:** Gilles Marcou

## Overview
This task involves compiling and integrating chemical hazard data from various sources. The objective is to create a comprehensive database that can be used for hazard assessment and to develop classification models for different hazards.

## Bibliography
- Collect and review about 20 relevant publications.
- Prepare a bundle of these publications for Crtomir to upload on GitLab.
- Key source: 
  - Vegosen L, Martin TM. An automated framework for compiling and integrating chemical hazard data. Clean Technol Environ Policy. 2020 Mar 1;22(2):441-458. doi: 10.1007/s10098-019-01795-w. PMID: 33867908; PMCID: PMC8048128.

## Software
- **KNIME**
- **Chemaxon**
- **ISIDA**
- **ISIDA/GTM**
- **Python** with standard libraries (scikit-learn, pandas, rdkit, indigo)
- **MOE**
- **Schrödinger**
- **Weka**

## Tasks

### Identification of Chemicals
- Extract chemical information from the source material.
- Ensure accurate identification and recording of chemicals.

### Data Organization
- Organize the collected data into a well-structured database.
- Define the architecture of the database to facilitate easy access and analysis.

### Standardization Protocol
- Develop and implement a protocol for standardizing chemical data.
- Ensure consistency and accuracy in data representation.

### Computation of Molecular Descriptors
- Compute molecular descriptors using various tools:
  - **ISIDA**
  - **MOE**
  - **Others** as necessary

### GTM (Generative Topographic Mapping)
- Develop and analyze GTM landscapes for visualizing data distributions.
- Utilize GTM for hazard classification.

### Classification Models
- Develop classification models for different types of chemical hazards.
- Use the standardized data and molecular descriptors to train these models.

## Deliverables
1. **Bibliography Bundle**
   - A bundle of about 20 relevant publications prepared for GitLab.
2. **Database**
   - A well-organized database of chemical hazard data.
3. **Standardization Protocol**
   - A documented protocol for standardizing chemical data.
4. **Molecular Descriptors**
   - Computed descriptors for all chemicals in the database.
5. **GTM Landscapes**
   - Visual representations of data distributions using GTM.
6. **Classification Models**
   - Trained models for predicting chemical hazards.

## Example Software Uses
- **KNIME** for data integration and workflow automation.
- **Chemaxon** for chemical informatics.
- **ISIDA** for molecular descriptor computation.
- **Python** (scikit-learn, pandas, rdkit, indigo) for data analysis and machine learning.
- **MOE** and **Schrödinger** for molecular modeling.
- **Weka** for additional machine learning tasks.

This task will contribute significantly to the development of a robust chemical hazard assessment framework by leveraging existing data and advanced computational tools.
