# Task 6: TET Enzymes - Ligand Preparation

## GROUP 6: TET - ligands
- Ho
- Davidov
- Afonso Costa
- Hunklinger
- **Main coordinator:** Dragos Horvath

## Overview
This task involves the preparation of ligands for TET enzymes by accessing various chemical databases, using specialized software, and performing a series of tasks aimed at composing a dataset of compounds with TET activity. The ultimate goal is to classify these activities and document the data source thoroughly.

## Databases Access
- **ChEMBL**
- **PubChem**
- **BindingDB**
- **IUPHAR**
- **SureChem**

## Software
- **Chemaxon**
- **KNIME**
- **Python** (libraries: Rdkit, Indigo)

## Tasks

### Compose a Dataset of Compounds with TET Activity
- Collect data from the specified databases.
- Compile a comprehensive dataset of compounds known to interact with TET enzymes.

### Classify Activities
- **Covalent / Non-covalent**: Differentiate between covalent and non-covalent interactions.
- **Allostery**: Identify compounds that act through allosteric mechanisms.
- **Activity Metrics**: Classify based on various activity metrics such as Ki, IC50, and general activity.

### Ontology of Biological Assays
- **Detection**: Define the methods used for detecting TET activity.
- **Incubation**: Document the incubation protocols used in the assays.
- **Sources**: Identify and document the sources of the biological assay data.

### Documentation
- Document the source of data for transparency and reproducibility.
- Download and store relevant PDFs for reference and further analysis.

### Database
- **Architecture of the Database**: Design the database structure to ensure it supports efficient data storage, retrieval, and analysis.
- **Standardization of Chemical Structures**: Implement protocols to standardize the chemical structures within the database to maintain consistency and accuracy.

## Deliverables
1. **Dataset of TET Compounds**
   - A well-curated dataset of compounds with known TET activity.
2. **Activity Classification**
   - A detailed classification of activities (covalent/non-covalent, allostery, Ki, IC50, etc.).
3. **Ontology of Biological Assays**
   - A comprehensive ontology covering detection methods, incubation protocols, and data sources.
4. **Documentation**
   - Thorough documentation of data sources and methods.
5. **Database**
   - A structured and standardized database ready for further analysis.

## Example Software Uses
- **Chemaxon** for chemical informatics and structure standardization.
- **KNIME** for data integration and workflow automation.
- **Python** (Rdkit, Indigo) for data processing and analysis.

This task aims to create a robust and comprehensive dataset of TET enzyme ligands, ensuring the data is well-organized, standardized, and documented for future research and analysis.
