# Task 3: ICHEM

## Group 3: ICHEM

- Blanc
- Gilmullin
- Panzeri
- Krüger
- **Main coordinator:** Guillaume

## Overview
This task focuses on various aspects of the ICHEM project, including debugging, deployment, and documentation. The primary goals are to ensure the software runs smoothly, prepare necessary documentation, and propose improvements or new features.

## Key Areas

### Debugging
- Identify and fix bugs.
- Ensure the software functions as expected.

### Deployment
- Prepare the software for deployment.
- Handle expected outdated binaries.
- Preferred language for deployment is C++.
- Use Python and KNIME for scripting.

### Testing
- Conduct "Unit" tests to ensure the reliability of different modules.

### Documentation
- Collect and prepare comprehensive documentation.
- Prepare documentation for the Command Line Interface (CLI).
- Include pre-requisites such as memory, CPU, GPU, system version, and required libraries.
- Provide installation instructions.
- Describe how to run the software.
- Outline steps to obtain and interpret results.

### Preparation
- Prepare targets before the hackathon.
- Prepare reference results during the hackathon.

## Tasks
1. **Reference Bugs**
   - Identify and document existing bugs.
2. **Complete the Documentation**
   - Ensure all aspects of the software are well-documented.
3. **Propose Tutorials**
   - Develop tutorials to help new users understand and use the software.
4. **Submit the Contribution to the Community**
   - Share your work and improvements with the broader community.

## Examples

### Binding Pocket Comparison
- Compare different binding pockets.

### Shaper
- Address dependency on OpenEye to solve issues.
- Provide clarity on ProCare and FuzzCav licenses and recompile the tools if necessary.
- Develop a tutorial based on Shaper 2014.

### Unit Testing with OpenEye
- Create unit tests using OpenEye.

### Open Science Alternatives
- Propose open science alternatives for the tutorial.

### Docking
- Investigate docking of CDK8 and ATK1.
- Assess merits of different docking score strategies.
- Use the tutorial from 2016 as a basis for development.
- Address the issue of the tool always outputting the same value (1.0).


