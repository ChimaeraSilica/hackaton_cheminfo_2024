# AVIAN FLU - NEURAMINIDASE
by Louise Carrat

Neuraminidase (NA) is a crucial enzyme for the replication cycle of the influenza virus. It acts by cleaving the glycosidic bonds of sialic acids, present on the surface of host cells and virions. This process facilitates the release of newly formed viral particles from infected cells and allows for the spread of infection.

Specifically, in the case of the H1N1 influenza virus, neuraminidase plays an essential role in the virulence and transmission of the virus. Infection with the H1N1 virus can lead to a range of symptoms, from mild to severe, and potentially fatal in some cases. Vulnerable populations include young children, the elderly, and those with underlying medical conditions.

Studies have shown that the neuraminidase of the H1N1 virus can be associated with severe respiratory complications, such as pneumonia, as well as damage to the lungs and respiratory system. Furthermore, the virus's ability to mutate rapidly can make the development of effective vaccines and treatments more challenging.

**References:**

1. Air, G. M. (2012). Influenza neuraminidase. Influenza and other respiratory viruses, 6(4), 245-256.
2. Krammer, F., & Palese, P. (2013). Influenza virus hemagglutinin and neuraminidase: Yin–Yang proteins coevolving to thwart immunity. Viruses, 5(4), 1021-1041.
3. Oxford, J. S., & Bossuyt, S. (2010). Balancing the risk of disease induced by influenza A/H1N1 vaccine: lessons learnt from narcolepsy. Expert review of vaccines, 9(5), 541-542.
4. Moscona, A. (2005). Neuraminidase inhibitors for influenza. New England Journal of Medicine, 353(13), 1363-1373.
5. Colman, P. M., & Varghese, J. N. (1983). Structure and function of the influenza virus neuraminidase. Biochimie, 65(7), 717-726.
6. Tung, L. T., Lin, Y. L., & Nguyen, T. H. (2019). N1 neuraminidase of H5N1 avian influenza A virus complexed with sialic acid and zanamivir - A study by molecular docking and molecular dynamics simulation. Journal of Molecular Graphics and Modelling, 91, 94-103.

## INHIBITOR OF NEURAMINIDASE

Oseltamivir and zanamivir are antiviral medications used to treat and prevent infections caused by the influenza virus by targeting neuraminidase, a viral enzyme essential for releasing newly formed viruses from infected cells. These medications act by selectively binding to neuraminidase, thereby inhibiting its enzymatic activity. As a result, the release of newly formed viruses is hindered, thus reducing the spread of the infection and helping alleviate flu symptoms.

**References:**

1. Jefferson T, Jones MA, Doshi P, Del Mar CB, Hama R, Thompson MJ, Spencer EA, Onakpoya I, Mahtani KR, Nunan D, Howick J, Heneghan CJ. Neuraminidase inhibitors for preventing and treating influenza in healthy adults and children. Cochrane Database Syst Rev. 2014;2014(4). doi: 10.1002/14651858.CD008965.pub4.
2. Samson M, Pizzorno A, Abed Y, Boivin G. Influenza virus resistance to neuraminidase inhibitors. Antiviral Res. 2013;98(2):174-185. doi: 10.1016/j.antiviral.2013.03.014.
3. Hossen MS, Barek MA, Jahan N, et al. Oseltamivir in combating viral infectious diseases other than influenza: A review. Cogent Biol. 2021;7(1):1948100. doi: 10.1080/23312025.2021.1948100.
4. Hien TT, de Jong M, Farrar J. Avian influenza—a challenge to global health care structures. N Engl J Med. 2004;351(23):2363-2365. doi: 10.1056/NEJMp048255.
5. Govorkova EA, Fang HB, Tan M, Webster RG. Neuraminidase inhibitor-rimantadine combinations exert additive and synergistic anti-influenza virus effects in MDCK cells. Antimicrob Agents Chemother. 2004;48(12):4855-4863. doi: 10.1128/AAC.48.12.4855-4863.2004.
6. Hurt AC. Oseltamivir resistance and the H274Y neuraminidase mutation in seasonal, pandemic and highly pathogenic influenza viruses. Drugs. 2009;69(18):2523-2531. doi: 10.2165/11531450-000000000-00000.

## MUTATION OF N1 AND THEIR CONSEQUENCES:

### H274Y (H1N1)
- **Implication:** This mutation significantly confers resistance to oseltamivir and peramivir in the N1 subtype. It is well-documented to reduce the effectiveness of oseltamivir (Tamiflu) while maintaining similar virulence to the wild-type (WT) virus in mice. This mutation generally does not affect sensitivity to zanamivir.

### N294S (H1N1 and H3N2)
- **Implication:** This mutation confers resistance to oseltamivir in N1 and N2 subtypes. In animal models, it has shown reduced virulence, with a higher LD50 and lower pulmonary viral titers compared to the WT virus.

### E119V (H1N1 and H3N2)
- **Implication:** In the N1 subtype, this mutation confers cross-resistance to oseltamivir, zanamivir, and peramivir, significantly increasing IC50 values compared to WT. In the N2 subtype, it primarily confers resistance to oseltamivir.

### R292K (H3N2)
- **Implication:** This mutation is associated with significant resistance to zanamivir and peramivir, and to a lesser extent, oseltamivir. It has been observed in clinical isolates and is concerning due to its ability to reduce the efficacy of multiple NAIs.

### E119G (H1N1 and H3N2)
- **Implication:** This mutation confers variable resistance to different NAIs depending on the subtype. In the N1 subtype, it can affect oseltamivir and zanamivir, while in the N2 subtype, the effects can differ based on the neuraminidase configuration.

### I223R (H1N1 and H3N2)
- **Implication:** This mutation is known to confer intermediate resistance to oseltamivir and potentially peramivir. It may also impact zanamivir sensitivity depending on the subtype.

### I222T (H1N1 and H3N2)
- **Implication:** The I222T mutation confers intermediate resistance to oseltamivir in both N1 and N2 subtypes. It can also increase resistance to peramivir and, in some cases, zanamivir. The presence of this mutation can contribute to a reduced overall susceptibility to NAIs, especially when combined with other mutations such as H274Y.

### S246N (H1N1 and H3N2)
- **Implication:** The S246N mutation is primarily associated with resistance to oseltamivir in both N1 and N2 subtypes. This mutation can also influence sensitivity to other NAIs, though its impact is often less pronounced compared to mutations like H274Y or R292K.

**Bibliography:**

1. Hurt AC, et al. "Susceptibility of highly pathogenic A(H5N1) avian influenza viruses to the neuraminidase inhibitors and adamantanes." Antiviral Res. 2007 Aug;74(2):228-231.
2. Monto, A. S., et al. (2006). "Detection of influenza viruses resistant to neuraminidase inhibitors in global surveillance during the first 3 years of their use." Antimicrobial Agents and Chemotherapy, 50(7), 2395-2402.
3. Gubareva, L. V., et al. "E119V and E119I mutations in influenza A (H1N1) and A (H3N2) neuraminidase: influence on virus susceptibility to different neuraminidase inhibitors." Journal of Virology, 72(6), 4769-4776.
4. McKimm-Breschkin, J. L., et al. (2013). "Neuraminidase inhibitor resistance in influenza viruses: a changing landscape." Journal of Antimicrobial Chemotherapy, 68(6), 1307-1316.
5. Gubareva, L. V., et al. "Neuraminidase inhibitor resistance in influenza viruses and laboratory testing methods." Antiviral Research, 2018, 156, 118-132.
6. Ito, M., et al. "Characterization of Drug-Resistant Influenza Virus A(H1N1) and A(H3N2) Variants Selected In Vitro with Laninamivir." Antimicrobial Agents and Chemotherapy, 2018, 62(7
